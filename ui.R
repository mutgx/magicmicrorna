library(shiny)

# Define UI
shinyUI(pageWithSidebar(
  
  # Application title
  headerPanel("MagiCMicroRna"),
 
  
   sidebarPanel(
	tags$img(HTML("<img src=https://bitbucket.org/mutgx/magicmicrorna/raw/master/screenshots/logo.png </img>")),
	p("Welcome to the MagiCMicroRna web application", br(), br()),  
	radioButtons("norm.method", p(strong(span("Type of normalization:", style="color:darkblue"))),
             c("None" = "none",
               "Quantile" = "quantile",
               "Scaled" = "scale",
               "RMA" = "rma",
			   "RMA with background correction" = "rmabg",
			   "RMA with background correction and quantile betweenArrays" = "rmaquantbg"),
			   selected="quantile"),
	p(br(), strong(span("Filtering options", style="color:darkblue"))),
	checkboxInput('matched', 'Separate miRNA-filtering for each group', TRUE),
	checkboxInput('controlfilter', 'Filter out control-spots', TRUE),
	checkboxInput('genedetected', 'Filter on flag "isGeneDetected"', TRUE),
	numericInput('genedetectedlim', 'Replicates detected (%)', 66, min=0, max=100, step=1), 
	checkboxInput('wellaboveneg', 'Filter on flag "wellAboveNEG"', FALSE),
	numericInput('wellaboveneglim', 'Replicates detected (%)', 33, min=0, max=100, step=1), 
	
	p(br(), strong(span("Data file upload", style="color:darkblue"))),
	p(em("Choose a ZIP-file that contains your datafiles (.txt) and targets.txt", style="font-size:10pt")),
	fileInput('file1', '',
              accept=c('application/zip', '.zip')),
    tags$hr(),
	actionButton('action', "Start ANALYSIS!")	
  ),
  
  
  mainPanel(
	tabsetPanel(
		tabPanel("Log", verbatimTextOutput('contents')),
		tabPanel("Download", uiOutput("download_button"))  
	)
  )
))